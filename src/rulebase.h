/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RULEBASE_H
#define RULEBASE_H

#include <set>
#include <string>

#include "localconsts.h"

class RuleBase
{
    public:
        RuleBase();

        virtual ~RuleBase()
        { }

        void init();

        virtual void start()
        { }

        virtual void end()
        { }

        virtual void parseLine(std::string data A_UNUSED)
        { }

        virtual void parseFile(std::string data A_UNUSED)
        { }

        const std::set<std::string> &getMasks() const
        { return mFileMasks; }

        void setFile(const std::string &file0)
        { file = file0; }

        void setLine(const int line0)
        { line = line0; }

        void setName(const std::string &name)
        { ruleName = name; }

        std::string getName() const A_WARN_UNUSED
        { return ruleName; }

        bool getFlag() const A_WARN_UNUSED
        { return flag; }

        void setRootDir(const std::string &dir)
        { rootDir = dir; }

        void terminateRule();

        void print(const std::string &text) const;

        void print(const std::string &text,
                   const int lineNumber) const;

        void printRaw(const std::string &text) const;

        std::string getFile() const
        { return file; }

    protected:
        void addMask(const std::string &mask);

        void deleteSelf();

        std::set<std::string> mFileMasks;

        std::string file;
        std::string ruleName;
        std::string rootDir;
        int line;
        bool flag;
};

#endif  // RULEBASE_H
