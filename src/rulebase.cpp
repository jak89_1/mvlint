/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rulebase.h"

#include "lintmanager.h"

#include "localconsts.h"

RuleBase::RuleBase() :
    file(),
    ruleName(),
    line(0),
    flag(true)
{
    lint.addRule(this);
}

void RuleBase::print(const std::string &text) const
{
    printf("[%s:%d]: V%s: %s\n",
        file.c_str(),
        line,
        ruleName.c_str(),
        text.c_str());
}

void RuleBase::print(const std::string &text,
                     const int lineNumber) const
{
    printf("[%s:%d]: V%s: %s\n",
        file.c_str(),
        lineNumber,
        ruleName.c_str(),
        text.c_str());
}

void RuleBase::printRaw(const std::string &text) const
{
    printf("%s\n", text.c_str());
}

void RuleBase::addMask(const std::string &mask)
{
    mFileMasks.insert(mask);
}

void RuleBase::deleteSelf()
{
    lint.deleteRule(this);
}

void RuleBase::terminateRule()
{
//    lint.deleteSelectedRule(this);
    flag = false;
}

void RuleBase::init()
{
    flag = true;
    line = 1;
}
