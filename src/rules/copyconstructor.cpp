/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "template.hpp"

registerRuleExt(copyConstructor, "016", "(.+)[.](cpp|h)")

namespace
{
    std::set<std::string> mClasses;
    std::map<std::string, int> mLines;
}  // namespace

startRule(copyConstructor)
{
    if (strEndWith(file, "debug/debug_new.cpp")
        || strEndWith(file, "debug/fast_mutex.h")
        || strEndWith(file, "debug/debug_new.h"))
    {
        terminateRule();
    }
}

endRule(copyConstructor)
{
    FOR_EACH(std::set<std::string>::const_iterator, it, mClasses)
    {
        print("Missing copy constructor marker A_DELETE_COPY / A_DEFAULT_COPY"
            " for class " + *it, mLines[*it]);
    }
    mClasses.clear();
    mLines.clear();
}

parseLineRule(copyConstructor)
{
    std::smatch m;
    if (isMatch(data, "(.*)(class|struct) ([a-zA-Z_0123456789]+)"
        " (|not)final(.*)",
        m))
    {
        const std::string str = m.str(3);
        mClasses.insert(str);
        mLines[str] = line;
    }
    else if (isMatch(data, "([ ]*)(A_DELETE_COPY|A_DEFAULT_COPY)[(]"
             "([a-zA-Z_0123456789]+)[)](.*)",
        m))
    {
        const std::string str = m.str(3);
        if (mClasses.find(str) != mClasses.end())
        {
            mClasses.erase(str);
            mLines.erase(str);
        }
        else
        {
            print("Wrong A_DELETE_COPY or A_DEFAULT_COPY marker");
        }
    }
}
