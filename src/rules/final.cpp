/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "template.hpp"

registerRuleExt(finalCheck, "007", "(.+)[.](cpp|h)")

startRule(finalCheck)
{
    if (strEndWith(file, "debug/debug_new.cpp")
        || strEndWith(file, "debug/fast_mutex.h")
        || strEndWith(file, "debug/debug_new.h"))
    {
        terminateRule();
    }
}

endRule(finalCheck)
{
}

parseLineRule(finalCheck)
{
    if (isMatch(data, "([ ]*)(static |)(class|struct) ([a-zA-Z_0123456789]+)"
        "($|( [:])([^;]+)(.*))"))
    {
        print("Need add final or notfinal into class declaration");
    }
}
