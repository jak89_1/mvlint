/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "template.hpp"

registerRuleExt(po, "010", "(.+)[.](po)")

bool readId(false);
bool readStr(false);
bool skipOneCheck(false);
std::string msgId;
std::string msgStr;

static int countStartSpaces(const std::string &str)
{
    const int sz = str.size();
    for (int f = 0; f < sz; f ++)
    {
        if (str[f] != ' ')
            return f;
    }
    return -1;
}

static void processMessage(RuleBase *const rule)
{
//    rule->print(std::string("pair: ").append(msgId).append(
//        "=").append(msgStr));
    readId = false;
    readStr = false;

    if (skipOneCheck)
    {
        skipOneCheck = false;
        return;
    }
    // skip not translated lines
    if (msgStr.empty())
        return;

    const size_t szId = msgId.size();
    const size_t szStr = msgStr.size();
    for (size_t f = 0; f < szId && f < szStr; f ++)
    {
        if (msgId[f] == ' ')
        {
            if (msgStr[f] != ' ')
            {
                rule->print("Wrong number of spaces at translation "
                    "line start.");
                break;
            }
        }
        break;
    }
    if (szId > 1 && szStr > 1)
    {
        const char sStr = msgStr[0];
        const char sId = msgId[0];
        const char eStr = msgStr[szStr - 1];
        const char eId = msgId[szId - 1];
        if (eStr == ' ' && eId != ' ')
            rule->print("Useless space at end of translation line.");
        std::string name = rule->getFile();
        if (findCutLast(name, "po/ar.po") || findCutLast(name, "po/fa.po"))
            return;
        if (name.find("po/unused/") != std::string::npos)
            return;

        bool isError(false);
        if (eId == '.' ||
            eId == ',' ||
            eId == '!' ||
            eId == '?' ||
            eId == '-' ||
            eId == ':' ||
            eId == ';' ||
            eId == '+' ||
            eId == '|' ||
            eId == '/' ||
            eId == ' ')
        {
            if (eId != eStr)
            {
                rule->print("Wrong character at end of translation line.");
                isError = true;
            }
        }
        if (sId == '.' ||
            sId == ',' ||
            sId == '!' ||
            sId == '?' ||
            sId == '-' ||
            sId == ':' ||
            sId == ';' ||
            sId == '+' ||
            sId == '|' ||
            sId == '/' ||
            sId == ' ')
        {
            if (sId != sStr)
            {
                rule->print("Wrong character at start of translation line.");
                isError = true;
            }
        }
        if (strStartWith(msgId, "\\\"") && strEndWith(msgId, "\\\"") &&
            !strStartWith(msgStr, "\\\"") && !strEndWith(msgStr, "\\\""))
        {
            rule->print("Missing \" at start and at end of translated line.");
            isError = true;
        }
        if (strStartWith(msgStr, "\\\"") && strEndWith(msgStr, "\\\"") &&
            !strStartWith(msgId, "\\\"") && !strEndWith(msgId, "\\\""))
        {
            rule->print("Extra \" at start and at end of translated line.");
            isError = true;
        }
        if (msgId.find("###") != std::string::npos &&
            msgStr.find("###") == std::string::npos)
        {
            rule->print("Missing ### in translation.");
        }
        if (isError == false)
        {
            if (countStartSpaces(msgId) != countStartSpaces(msgStr))
                rule->print("Wrong number of spaces at start of translation line.");
        }
/*
        if (msgId.find("\\n") != std::string::npos)
        {
            if (!findCutLast(name, "help/zh_CN.po"))
            {
                if (msgStr.find("\\n") == std::string::npos)
                    rule->print("Missing \\n in translation.");
            }
        }
        else
        {
            if (msgStr.find("\\n") != std::string::npos)
                rule->print("Useless \\n in translation.");
        }
*/
    }
}

startRule(po)
{
    if (strEndWith(file, "po/el.po"))
    {
        terminateRule();
        return;
    }

    readId = false;
    readStr = false;
    skipOneCheck = false;
    msgId = std::string();
    msgStr = std::string();
}

endRule(po)
{
    if (readStr)
        processMessage(this);
}

parseLineRule(po)
{
    if (strStartWith(data, "#, fuzzy"))
    {
        skipOneCheck = true;
    }
    else if (findCutFirst(data, "msgid \""))
    {   // msgId start
        if (readStr)
        {
            processMessage(this);
            readStr = false;
        }
        readId = true;
        if (findCutLast(data, "\""))
            msgId = data;
        else
            print("Wrong msgId line. Missing last \".");
    }
    else if (findCutFirst(data, "msgstr \"") ||
             findCutFirst(data, "msgstr[0] \""))
    {   // msgStr start
        if (readId)
            readId = false;
        readStr = true;
        if (findCutLast(data, "\""))
            msgStr = data;
        else
            print("Wrong msgStr line. Missing last \".");
    }
    else if (findCutFirst(data, "msgid_plural \"") ||
             findCutFirst(data, "msgstr["))
    {
        // nothing
    }
    else if (findCutFirst(data, "\""))
    {   // line start with "
        if (readId)
        {
            if (findCutLast(data, "\""))
                msgId += data;
            else
                print("Wrong msgId line. Missing last \".");
        }
        if (readStr)
        {
            if (findCutLast(data, "\""))
                msgStr += data;
            else
                print("Wrong msgStr line. Missing last \".");
        }
    }
    else
    {   // other lines
        if (readStr)
            processMessage(this);
        readId = false;
        readStr = false;
        msgId = std::string();
        msgStr = std::string();
    }
}

