/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "template.hpp"

registerRuleExt(baseclass, "006", "(.+)[.](cpp|h)")

startRule(baseclass)
{
}

endRule(baseclass)
{
}

parseLineRule(baseclass)
{
    std::smatch m;
    if (data.find("friend ") == std::string::npos &&
        isMatch(data, "(.*)(class|struct) (([a-zA-Z_0123456789]+)|"
        "([a-zA-Z_0123456789 ]+)([a-zA-Z_0123456789]+))[:](.*)",
        m))
    {
        print("Wrong align after class name and before ':'.");
    }
}
