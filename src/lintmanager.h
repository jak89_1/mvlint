/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LINTMANAGER_H
#define LINTMANAGER_H

#include <string>
#include <vector>

#include "localconsts.h"

class RuleBase;

class LintManager final
{
    public:
        LintManager();

        void run(std::string path);

        void addRule(RuleBase *const rule);

        void deleteRule(RuleBase *const rule);

        void deleteSelectedRule(RuleBase *const rule);

    protected:
        void enumFiles(std::string path);

        void processFile(std::string fileName);

        bool isMatchFile(RuleBase *const rule);

        void readFile();

        void selectRulesForFile();

        void applyRulesToFile();

        void deleteFrom(RuleBase *const rule,
                        std::vector<RuleBase*> &rules);

        void updateRules();

    private:
        std::vector<RuleBase*> mRules;

        std::vector<RuleBase*> mSelectedRules;

        std::vector<std::string> mFileData;

        std::string mFileName;

        std::string mRootPath;
};

extern LintManager lint;

#endif  // LINTMANAGER_H
