/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lintmanager.h"

#include "debug.h"

class Test1 final
{
public:
    A_DELETE_COPY(Test3);
    Test1::Test1()
    {
    }
}

struct Test2 final
{
protected:
    Test2::Test2() :
        data1(),
        data2()
    {
    }
}

struct Test3 final
{
        private:
    Test3::Test3() :
        data1(),
        data2()
    {
    }
}
